#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""Python Sudoku is a text and graphical program (gtk interface) to create or
resolve sudokus. It can also print a sudoku (1 or 4 sudokus in each page) and
write an image (png, jpeg, etc) with a sudoku.

For usage: pysdk-image.py --help


Copyright (C) 2005-2008  Xosé Otero <xoseotero@users.sourceforge.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


Modification history:
2005/10/12  Antti Kuntsi
	Added options to set the region size.

"""

import sys
import os
import traceback
import optparse
# Python Sudoku internacionalization
import locale
import gettext


import codecs

# Wrap sys.stdout and sys.stderr is needed for optparse that use
# sys.stdout.write instead of print
# This was fixed in python version 2.5 and is not needed any more.
if sys.version_info[0] <= 2 and sys.version_info[1] <= 4:
    if sys.stdout.encoding:
        sys.stdout = codecs.getwriter(sys.stdout.encoding)(sys.stdout)
    if sys.stderr.encoding:
        sys.stderr = codecs.getwriter(sys.stderr.encoding)(sys.stderr)

# Wrap sys.stdout and sys.stderr is needed when is redirected
if not sys.stdout.encoding:
    sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout)
if not sys.stderr.encoding:
    sys.stderr = codecs.getwriter(locale.getpreferredencoding())(sys.stderr)


from pythonsudoku.config import localedir, options
locale.setlocale(locale.LC_ALL, '')
l10n = gettext.translation("pythonsudoku", localedir, fallback=True)
l10n.install(True)


from pythonsudoku.board import Board
from pythonsudoku.info import Info
from pythonsudoku.image import Image


# send bugs here
BUG_URL = "http://sourceforge.net/tracker/?group_id=150356&atid=778307"


def whatis():
    """Print what is a sudoku."""
    print Info["whatis"]

def create_parser():
    """Create a optparse.OptionParser with all the Python Sudoku options."""
    usage = _(u"""
  %prog [Image Options] INPUT.sdk OUTPUT.format
  %prog --version | -h | -m | -w

  INPUT.sdk is a Python Sudoku file
  OUTPUT.format is an image file""")
    version = "%prog " + Info["version"]
    parser = optparse.OptionParser(usage=usage, version=version)
    parser.add_option("-w", "--whatis",
                      action="store_true", dest="info", default=False,
                      help=_(u"information about what is sudoku"))


    group = optparse.OptionGroup(parser, _(u"Image Options"))
    group.add_option("--format",
                     action="store", type="string", dest="format",
                     default=options.get("image", "format"),
                     help=_(u"set the image format (\"png\", \"jpeg\", etc) (\"%s\" is the default)") % options.get("image", "format"))
    group.add_option("--width",
                     action="store", type="int", dest="width",
                     default=options.getint("image", "width"),
                     help=_(u"set the image width in pixels (%d is the default)") % options.getint("image", "width"))
    group.add_option("--height",
                     action="store", type="int", dest="height",
                     default=options.getint("image", "height"),
                     help=_(u"set the image height in pixels (%d is the default)") % options.getint("image", "height"))
    group.add_option("--no_background",
                     action="store_true", dest="no_background",
                     default=False,
                     help=_(u"create a transparent image without background (if the format doesn't support transparency a black background is created)"))
    group.add_option("--background",
                     action="store", type="string", dest="background",
                     default=options.get("image", "background"),
                     help=_(u"set the image background (\"white\", \"blue\", etc) (\"%s\" is the default)") % options.get("image", "background"))

    group.add_option("--lines",
                     action="store", type="string", dest="lines",
                     default=options.get("image", "lines_colour"),
                     help=_(u"set the lines colour (\"black\", \"blue\", etc) (\"%s\" is the default)") % options.get("image", "lines_colour"))
    group.add_option("--font",
                     action="store", type="string", dest="font",
                     default=options.get("image", "font"),
                     help=_(u"set the font for the numbers (absolute path or relative to the script) (\"%s\" is the default)") % options.get("image", "font"))
    group.add_option("--font_size",
                     action="store", type="int", dest="font_size",
                     default=options.getint("image", "font_size"),
                     help=_(u"set the font size for the numbers (%d is the default)") % options.getint("image", "font_size"))
    group.add_option("--font_colour",
                     action="store", type="string", dest="font_colour",
                     default=options.get("image", "font_colour"),
                     help=_(u"set the font colour for the numbers (\"black\", \"blue\", etc) (\"%s\" is the default)") % options.get("image", "font_colour"))
    parser.add_option_group(group)

    group = optparse.OptionGroup(parser, _(u"Visualization Options"))
    if options.getboolean("sudoku", "use_letters"):
        help_use_letters = _(u"show letters instead of numbers > 9 (default)")
        help_numbers_only = _(u"show only numbers")
    else:
        help_use_letters = _(u"show letters instead of numbers > 9")
        help_numbers_only = _(u"show only numbers (default)")
    group.add_option("--use_letters",
                     action="store_true", dest="use_letters",
                     default=options.getboolean("sudoku", "use_letters"),
                     help=help_use_letters)
    group.add_option("--numbers_only",
                     action="store_false", dest="use_letters",
                     default=not options.getboolean("sudoku", "use_letters"),
                     help=help_numbers_only)
    parser.add_option_group(group)

    return parser

def check_args(parser, options, args):
    """Check if the number of arguments if correct.

    Arguments:
    parser -- the optparse.OptionParser
    options -- then options returned by optparse.OptionParser.parser_args()
    args -- the args returned by options returned by
            optparse.OptionParser.parser_args()

    """
    length = len(args)

    if length != 2:
        parser.error(_(u"incorrect number of arguments"))

def set_options(opts):
    global options

    # 3rd param of SafeConfigParser.set() must be a string
    options.set("sudoku", "use_letters", str(opts.use_letters))

    options.set("image", "format", opts.format)
    options.set("image", "width", str(opts.width))
    options.set("image", "height", str(opts.height))
    if opts.no_background:
        options.set("image", "background", "")
    else:
        options.set("image", "background", opts.background)
    options.set("image", "lines_colour", opts.lines)
    options.set("image", "font", opts.font)
    options.set("image", "font_colour", opts.font_colour)
    options.set("image", "font_size", str(opts.font_size))


def main():
    parser = create_parser()
    (opts, args) = parser.parse_args()
    check_args(parser, opts, args)
    set_options(opts)

    try:
        import psyco
        psyco.full()
    except ImportError:
        pass

    if opts.info:
        whatis()
    else:                               # create image
        Image(Board(filename=args[0]), args[1])

    sys.exit()

if __name__ == "__main__":
    try:
        main()
    except (SystemExit, KeyboardInterrupt):
        pass
    except:
        print >> sys.stderr, _(u"\t-- cut here --")
        traceback.print_exc()
        print >> sys.stderr, _(u"\t-- cut here --")
        print >> sys.stderr
        print >> sys.stderr, _(u"An error has happened, please go to %s and send a bug report with the last lines.") % (BUG_URL)
